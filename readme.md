# Demo Project: #
- Create server and deploy application on DigitalOcean

# Technologies used: #
- DigitalOcean
- Linux 
- Java 
- Gradle

# Project Description: #
1. ## Setup and configure a server on DigitalOcean ##
  - configure an ssh key
  - configure a firewall
2. ## Create and configure a new Linux user on the Droplet (Security best practice) ##
  - adduser chol
  - usermod -aG sudo chol
  - su - chol
  - sudo vim .ssh/authorized_keys - place public ssh key here
3. ## Deploy and run a Java Gradle application on Droplet ##
  - apt update
  - apt install openjdk-8-jre-headless
  - gradle build - run this in the project folder
  - scp *.jar root@*:/root
  # On the Server #
  - java -jar  *.jar & - to run the application
  - configure the port the application is running on in **droplet firewall**

# Module 5: Cloud & Infrastructure as Service Basics #





# Java-React Example

An example of how to use JS frontend to consume an endpoint written in Java.

## Frontend technologies

- [React](https://facebook.github.io/react/) - UI Library
- [Redux](http://redux.js.org/) - State container

## Additional information

This project is a part of a [presentation](https://docs.google.com/presentation/d/1-yZhsM43cyWWDVn6EUtK_wc39FAv-19_jwsKXlTe2o8/edit?usp=sharing)

Related projects:

- [react-intro](https://github.com/mendlik/react-intro) - Introduction to react and redux.
- [java-webpack-example](https://github.com/mendlik/java-webpack-example) - Advanced example showing how to use a module bundler in  a Java project.

Tip: [How to enable LiveReload in IntelliJ](http://stackoverflow.com/a/35895848/2284884)

This project uses gradle version: 7.4

<hr/>
Original project can be found here: https://github.com/pmendelski/java-react-example 
